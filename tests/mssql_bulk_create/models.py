from django.db import models


class FiveFields(models.Model):
    f1 = models.IntegerField(unique=True)
    f2 = models.IntegerField(unique=True)
    f3 = models.IntegerField(unique=True)
    f4 = models.IntegerField(unique=True)
    f5 = models.IntegerField(unique=True)
