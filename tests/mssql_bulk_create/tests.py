from __future__ import absolute_import

from django.db import connection
from django.test import TestCase
from django.test.utils import override_settings

from .models import FiveFields


class BulkCreateTests(TestCase):
    def test_too_many_parameters(self):
        # This test fails if all 800 entries are inserted in a single query.
        # To comply with the 2100 parameters limit imposed by SQL Server,
        # at most 420 entries should be sent per query.
        with override_settings(DEBUG=True):
            connection.queries = []
            FiveFields.objects.bulk_create([
                   FiveFields(f1=i+1, f2=i+2, f3=i+3, f4=i+4, f5=i+5) for i in range(0, 800)
                ])
            query_count = len(connection.queries)
        self.assertEqual(query_count, 2)
        self.assertEqual(FiveFields.objects.count(), 800)

    def test_primary_keys(self):
        # Asserts whether primary keys are set in model objects saved with bulk_create.
        objects = [FiveFields(f1=i + 1, f2=i + 2, f3=i + 3, f4=i + 4, f5=i + 5) for i in range(0, 300)]
        with override_settings(DEBUG=True):
            connection.queries = []
            FiveFields.objects.bulk_create(objects, set_primary_keys=True)
            query_count = len(connection.queries)
        self.assertEqual(query_count, 1)
        self.assertIsNotNone(objects[0].pk)
        self.assertEqual(objects[1].f3, FiveFields.objects.get(pk=objects[1].pk).f3)
