from __future__ import unicode_literals


#25894 Evaluation of zero-length slices of queryset.values() fails
def set_limits(self, low=None, high=None):
    self._original_set_limits(low, high)
    if self.low_mark == self.high_mark:
        self.set_empty()

from django.db.models.sql.query import Query
Query._original_set_limits = Query.set_limits
Query.set_limits = set_limits

from django.db import connections, transaction
from django.db.models.fields import AutoField
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.utils.functional import partition

def bulk_create(self, objs, batch_size=None, set_primary_keys=False, fix_foreign_keys=False):
    '''
    Changes bulk_create to accept two additional parameters: set_primary_keys and fix_foreign_keys.
    Source code copied and adapter from Django 1.8.17: django.db.models.query.QuerySet.bulk_create

    set_primary_keys:
    - If False, behavior is the current (as of Django 1.8.17) behavior: no primary keys will be set in saved objects.
    - If True, it saves the given objects in the fastest way possible that allows PKs to be retrieved;
    Depending on the underlying database engine, it may be implemented as bulk insert(s) or one insert per object.
    Inspired by Django ticket https://code.djangoproject.com/ticket/19527 back when it was being considered
    for Django 1.8. Later it got bumped to Django 1.10.

    fix_foreign_keys:
    - If False, nothing is changed from the default behavior.
    - If True, all objects' foreign key fields are re-set to fix FK references of related objects that
      had no PKs at the time the fields were set, and were saved afterwards (a common case when the related objects
      were created with bulk_create(set_primary_keys=True). See: https://code.djangoproject.com/ticket/8892
    - If an iterable (e.g. a list), only the given fields will be checked, rather than all FK fields.
      If you already know the fields that need fixing you can specify them to speed up the process.
    '''
    assert batch_size is None or batch_size > 0
    if not objs:
        return objs
    self._for_write = True
    connection = connections[self.db]
    parents = self.model._meta.parents.keys()

    if fix_foreign_keys:
        if hasattr(fix_foreign_keys, '__iter__'):
            foreign_key_fields = []
            for name in fix_foreign_keys:
                f = self.model._meta.get_field_by_name(name)[0]
                foreign_key_fields.append((f.name, f.column))
        else:
            foreign_key_fields = [(f.name, f.column) for f in self.model._meta.fields if isinstance(f, ForeignKey)]
        for obj in objs:
            for field, column in foreign_key_fields:
                value = getattr(obj, field)
                if getattr(obj, column) is None and value is not None:
                    setattr(obj, field, value)
    
    for parent in parents:
        parent_field_name = self.model._meta.parents[parent].attname
        parent_objs_to_create = []
        for obj in objs:
            parent_obj = parent(**{f.attname: getattr(obj, f.attname) for f in parent._meta.local_concrete_fields if not f.primary_key})
            setattr(obj, parent_field_name, parent_obj)
            parent_objs_to_create.append(parent_obj)
        bulk_create(parent.objects.get_queryset(), parent_objs_to_create, batch_size=batch_size, set_primary_keys=True, fix_foreign_keys=fix_foreign_keys)
        for obj in objs:
            setattr(obj, parent_field_name, getattr(obj, parent_field_name).pk)
            
    if not self.model._meta.has_auto_field:
        set_primary_keys = False

    if set_primary_keys and not getattr(connection.features, 'can_return_id_from_bulk_insert', False):
        # Engine does not support returning PKs from bulk insert. Save items one by one.
        for obj in objs:
            obj.save()
        return

    fields = self.model._meta.local_concrete_fields
    objs = list(objs)
    self._populate_pk_values(objs)
    with transaction.atomic(using=self.db, savepoint=False):
        if (connection.features.can_combine_inserts_with_and_without_auto_increment_pk
                and self.model._meta.has_auto_field):
            self._batched_insert(objs, fields, batch_size, set_primary_keys=set_primary_keys)
        else:
            objs_with_pk, objs_without_pk = partition(lambda o: o.pk is None, objs)
            if objs_with_pk:
                self._batched_insert(objs_with_pk, fields, batch_size)
            if objs_without_pk:
                fields = [f for f in fields if not isinstance(f, AutoField)]
                self._batched_insert(objs_without_pk, fields, batch_size, set_primary_keys=set_primary_keys)

    return objs


def _batched_insert(self, objs, fields, batch_size, set_primary_keys=False):
    # Changes _batched_insert to optionally set objects' primary keys.
    # Copied and adapted from Django 1.8.17 source code: django.db.models.query.QuerySet._batched_insert
    if not objs:
        return
    pk_field_name = self.model._meta.pk.attname
    ops = connections[self.db].ops
    batch_size = (batch_size or max(ops.bulk_batch_size(fields, objs), 1))
    for batch in [objs[i:i + batch_size]
                  for i in range(0, len(objs), batch_size)]:
        batch_ids = self.model._base_manager._insert(batch, fields=fields, using=self.db, return_id=set_primary_keys)
        if set_primary_keys:
            if len(batch) == 1:
                batch_ids = [batch_ids]
            for i, id_ in enumerate(batch_ids):
                setattr(batch[i], pk_field_name, id_)

QuerySet.bulk_create = bulk_create
QuerySet._batched_insert = _batched_insert
